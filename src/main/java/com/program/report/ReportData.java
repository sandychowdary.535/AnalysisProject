package com.program.report;

import com.program.pojo.SortData;

public class ReportData {

	SortData inc100Results = new SortData();
	SortData dec100Results = new SortData();
	SortData Ran100Results = new SortData();

	SortData inc1000Results = new SortData();
	SortData dec1000Results = new SortData();
	SortData Ran1000Results = new SortData();

	SortData inc10000Results = new SortData();
	SortData dec10000Results = new SortData();
	SortData Ran10000Results = new SortData();

	SortData inc30000Results = new SortData();
	SortData dec30000Results = new SortData();
	SortData Ran30000Results = new SortData();

	public SortData getInc100Results() {
		return inc100Results;
	}

	public void setInc100Results(SortData inc100Results) {
		this.inc100Results = inc100Results;
	}

	public SortData getDec100Results() {
		return dec100Results;
	}

	public void setDec100Results(SortData dec100Results) {
		this.dec100Results = dec100Results;
	}

	public SortData getRan100Results() {
		return Ran100Results;
	}

	public void setRan100Results(SortData ran100Results) {
		Ran100Results = ran100Results;
	}

	public SortData getInc1000Results() {
		return inc1000Results;
	}

	public void setInc1000Results(SortData inc1000Results) {
		this.inc1000Results = inc1000Results;
	}

	public SortData getDec1000Results() {
		return dec1000Results;
	}

	public void setDec1000Results(SortData dec1000Results) {
		this.dec1000Results = dec1000Results;
	}

	public SortData getRan1000Results() {
		return Ran1000Results;
	}

	public void setRan1000Results(SortData ran1000Results) {
		Ran1000Results = ran1000Results;
	}

	public SortData getInc10000Results() {
		return inc10000Results;
	}

	public void setInc10000Results(SortData inc10000Results) {
		this.inc10000Results = inc10000Results;
	}

	public SortData getDec10000Results() {
		return dec10000Results;
	}

	public void setDec10000Results(SortData dec10000Results) {
		this.dec10000Results = dec10000Results;
	}

	public SortData getRan10000Results() {
		return Ran10000Results;
	}

	public void setRan10000Results(SortData ran10000Results) {
		Ran10000Results = ran10000Results;
	}

	public SortData getInc30000Results() {
		return inc30000Results;
	}

	public void setInc30000Results(SortData inc30000Results) {
		this.inc30000Results = inc30000Results;
	}

	public SortData getDec30000Results() {
		return dec30000Results;
	}

	public void setDec30000Results(SortData dec30000Results) {
		this.dec30000Results = dec30000Results;
	}

	public SortData getRan30000Results() {
		return Ran30000Results;
	}

	public void setRan30000Results(SortData ran30000Results) {
		Ran30000Results = ran30000Results;
	}

}
