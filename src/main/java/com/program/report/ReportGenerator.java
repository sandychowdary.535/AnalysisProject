package com.program.report;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReportGenerator {
	private static XSSFWorkbook xssfWorkbook;
	private static XSSFCellStyle style,style1,style2;
	private static Row row;
	private static Cell cell;
	private static XSSFFont hssfFont;

	public ReportGenerator() {
	}

	public static void gernerate(Map<String, ReportData> values) {
		xssfWorkbook = new XSSFWorkbook();
		XSSFSheet xssfSheet = generateSheet(xssfWorkbook, "comp");
		createCompSheet(values, xssfSheet);
		xssfSheet = generateSheet(xssfWorkbook, "time");
		createTimingSheet(values, xssfSheet);
		createExcelSheet();
	}

	private static XSSFSheet generateSheet(XSSFWorkbook xssfWorkbook, String name) {

		XSSFSheet xssfSheet = xssfWorkbook.createSheet(name);
		xssfSheet.setColumnWidth(0, 19900);
		xssfSheet.setColumnWidth(1, 19900);

		// creating Main Heading
		xssfSheet.addMergedRegion(new CellRangeAddress(0, 0, 3,15));
		
		style = xssfWorkbook.createCellStyle();
		hssfFont = xssfWorkbook.createFont();
		hssfFont.setBold(true);
		hssfFont.setFontHeightInPoints((short) 22);
		hssfFont.setColor(HSSFColor.GREEN.index);
		style.setWrapText(true);
		style.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
		style.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		style.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		style.setFont(hssfFont);
		
		style1 = xssfWorkbook.createCellStyle();
		hssfFont = xssfWorkbook.createFont();
		hssfFont.setBold(true);
		hssfFont.setFontHeightInPoints((short) 16);
		hssfFont.setColor(HSSFColor.RED.index);
		style1.setWrapText(true);
		style1.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
		style1.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		style1.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		style1.setFont(hssfFont);
		
		style2 = xssfWorkbook.createCellStyle();
		hssfFont = xssfWorkbook.createFont();
		hssfFont.setBold(false);
		hssfFont.setFontHeightInPoints((short) 14);
		hssfFont.setColor(HSSFColor.BLACK.index);
		style2.setWrapText(true);
		style2.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
		style2.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		style2.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		style2.setFont(hssfFont);
		
		
		row = xssfSheet.createRow(0);
		row.setHeight((short) 500);
		cell = row.createCell(3);
		if (name == "comp")
			cell.setCellValue("Analysizing comparison counts of Sorting algorithems");
		else if (name == "time")
			cell.setCellValue("Analysizing Timing of Sorting algorithems");
		cell.setCellStyle(style);

		// creating INFO and DEBUG Heading
		style = xssfWorkbook.createCellStyle();
		hssfFont = xssfWorkbook.createFont();
		hssfFont.setBold(true);
		hssfFont.setFontHeightInPoints((short) 14);
		hssfFont.setColor(HSSFColor.BLUE.index);

		style.setWrapText(true);
		style.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		style.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		style.setFont(hssfFont);

		xssfSheet.addMergedRegion(new CellRangeAddress(1, 1, 1, 3));
		xssfSheet.addMergedRegion(new CellRangeAddress(1, 1, 4, 6));
		xssfSheet.addMergedRegion(new CellRangeAddress(1, 1, 7, 9));
		xssfSheet.addMergedRegion(new CellRangeAddress(1, 1, 10, 12));

		row = xssfSheet.createRow(1);

		cell = row.createCell(0);
		cell.setCellValue("Array Size");
		cell.setCellStyle(style);

		cell = row.createCell(1);
		cell.setCellValue("100");
		cell.setCellStyle(style);

		cell = row.createCell(4);
		cell.setCellValue("1000");
		cell.setCellStyle(style);

		cell = row.createCell(7);
		cell.setCellValue("10000");
		cell.setCellStyle(style);

		cell = row.createCell(10);
		cell.setCellValue("30000");
		cell.setCellStyle(style);

		row = xssfSheet.createRow(2);

		cell = row.createCell(0);
		cell.setCellValue("Array Type");
		cell.setCellStyle(style);

		for (int i = 1; i <= 12; i = i + 3) {
			cell = row.createCell(i);
			cell.setCellValue("INC");
			cell.setCellStyle(style1);

			cell = row.createCell(i + 1);
			cell.setCellValue("DEC");
			cell.setCellStyle(style1);

			cell = row.createCell(i + 2);
			cell.setCellValue("RAN");
			cell.setCellStyle(style1);
		}
		for (int i = 3; i <= 10; i++) {
			row = xssfSheet.createRow(i);
			String value = null;
			switch (i) {
			case 3:
				value = "Selection";
				break;
			case 4:
				value = "Bubble";
				break;
			case 5:
				value = "Insertion";
				break;
			case 6:
				value = "Modified Insertion";
				break;
			case 7:
				value = "Quick";
				break;
			case 8:
				value = "Randomized Quick";
				break;
			case 9:
				value = "Merge";
				break;
			case 10:
				value = "Heap";
				break;

			}
			cell = row.createCell(0);
			cell.setCellValue(value);
			cell.setCellStyle(style);
		}
		return xssfSheet;

	}

	private static void createTimingSheet(Map<String, ReportData> values, XSSFSheet xssfSheet) {

		for (int i = 3; i <= 10; i++) {
			row = xssfSheet.getRow(i);
			String value = null;
			switch (i) {
			case 3:
				value = "selection";
				break;
			case 4:
				value = "bubble";
				break;
			case 5:
				value = "insertion";
				break;
			case 6:
				value = "modifiedInsertion";
				break;
			case 7:
				value = "quick";
				break;
			case 8:
				value = "randomizedQuick";
				break;
			case 9:
				value = "merge";
				break;
			case 10:
				value = "heap";
				break;

			}

			ReportData data = values.get(value);
			cell = row.createCell(1);
			cell.setCellValue(data.getInc100Results().getTime());
			cell.setCellStyle(style2);

			cell = row.createCell(2);
			cell.setCellValue(data.getDec100Results().getTime());
			cell.setCellStyle(style2);

			cell = row.createCell(3);
			cell.setCellValue(data.getRan100Results().getTime());
			cell.setCellStyle(style2);

			cell = row.createCell(4);
			cell.setCellValue(data.getInc1000Results().getTime());
			cell.setCellStyle(style2);

			cell = row.createCell(5);
			cell.setCellValue(data.getDec1000Results().getTime());
			cell.setCellStyle(style2);

			cell = row.createCell(6);
			cell.setCellValue(data.getRan1000Results().getTime());
			cell.setCellStyle(style2);

			cell = row.createCell(7);
			cell.setCellValue(data.getInc10000Results().getTime());
			cell.setCellStyle(style2);

			cell = row.createCell(8);
			cell.setCellValue(data.getDec10000Results().getTime());
			cell.setCellStyle(style2);

			cell = row.createCell(9);
			cell.setCellValue(data.getRan10000Results().getTime());
			cell.setCellStyle(style2);

			cell = row.createCell(10);
			cell.setCellValue(data.getInc30000Results().getTime());
			cell.setCellStyle(style2);

			cell = row.createCell(11);
			cell.setCellValue(data.getDec30000Results().getTime());
			cell.setCellStyle(style2);

			cell = row.createCell(12);
			cell.setCellValue(data.getRan30000Results().getTime());
			cell.setCellStyle(style2);
		}

	}

	private static void createCompSheet(Map<String, ReportData> values, XSSFSheet xssfSheet) {
		for (int i = 3; i <= 10; i++) {
			row = xssfSheet.getRow(i);
			String value = null;
			switch (i) {
			case 3:
				value = "selection";
				break;
			case 4:
				value = "bubble";
				break;
			case 5:
				value = "insertion";
				break;
			case 6:
				value = "modifiedInsertion";
				break;
			case 7:
				value = "quick";
				break;
			case 8:
				value = "randomizedQuick";
				break;
			case 9:
				value = "merge";
				break;
			case 10:
				value = "heap";
				break;

			}

			ReportData data = values.get(value);
			cell = row.createCell(1);
			cell.setCellValue(data.getInc100Results().getComp());
			cell.setCellStyle(style2);

			cell = row.createCell(2);
			cell.setCellValue(data.getDec100Results().getComp());
			cell.setCellStyle(style2);

			cell = row.createCell(3);
			cell.setCellValue(data.getRan100Results().getComp());
			cell.setCellStyle(style2);

			cell = row.createCell(4);
			cell.setCellValue(data.getInc1000Results().getComp());
			cell.setCellStyle(style2);

			cell = row.createCell(5);
			cell.setCellValue(data.getDec1000Results().getComp());
			cell.setCellStyle(style2);

			cell = row.createCell(6);
			cell.setCellValue(data.getRan1000Results().getComp());
			cell.setCellStyle(style2);

			cell = row.createCell(7);
			cell.setCellValue(data.getInc10000Results().getComp());
			cell.setCellStyle(style2);

			cell = row.createCell(8);
			cell.setCellValue(data.getDec10000Results().getComp());
			cell.setCellStyle(style2);

			cell = row.createCell(9);
			cell.setCellValue(data.getRan10000Results().getComp());
			cell.setCellStyle(style2);

			cell = row.createCell(10);
			cell.setCellValue(data.getInc30000Results().getComp());
			cell.setCellStyle(style2);

			cell = row.createCell(11);
			cell.setCellValue(data.getDec30000Results().getComp());
			cell.setCellStyle(style2);

			cell = row.createCell(12);
			cell.setCellValue(data.getRan30000Results().getComp());
			cell.setCellStyle(style2);
		}

	}

	private static void createExcelSheet() {
		try {
			FileOutputStream out = new FileOutputStream(new File("reports/test.xlsx"));
			xssfWorkbook.write(out);
			out.close();
			System.out.println("Excel written successfully..");

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
