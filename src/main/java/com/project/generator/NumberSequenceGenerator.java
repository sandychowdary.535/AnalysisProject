package com.project.generator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class NumberSequenceGenerator {

	public static int[] randomSequence(int limit) {
		Integer[] temp = new Integer[limit];
		List<Integer> list = new ArrayList<Integer>();
		for (int i = 0; i < limit; i++) {
			list.add(i + 1);
		}
		Collections.shuffle(list);
		temp = list.toArray(temp);
		int[] elements = Arrays.stream(temp).mapToInt(Integer::intValue).toArray();
		return elements;
	}

	public static int[] increasingOrder(int limit) {
		int[] elements = new int[limit];
		for (int i = 0; i < limit; i++) {
			elements[i] = i + 1;
		}
		return elements;
	}

	public static int[] decreasingOrder(int limit) {
		int[] elements = new int[limit];
		int x = 0;
		for (int i = limit - 1; i >= 0; i--) {
			elements[x++] = limit--;
		}
		return elements;
	}

	public static void main(String[] args) {

		int limit = 100;
		int[] a = NumberSequenceGenerator.randomSequence(limit);
		int[] b = NumberSequenceGenerator.increasingOrder(limit);
		int[] c = NumberSequenceGenerator.decreasingOrder(limit);
		
		for (int i = 0; i < a.length; i++) {
			System.out.print(a[i] + "  ");
		}
		System.out.println();
		for (int i = 0; i < b.length; i++) {
			System.out.print(b[i] + "  ");
		}
		System.out.println();
		for (int i = 0; i < c.length; i++) {
			System.out.print(c[i] + "  ");
		}
	}
}
