package com.project.sortings;

import com.program.report.ReportData;

public interface Sort {

	abstract void sortElements();

	void printElements();

	void printTotalRunningTime(int limit);

	void printTotalComparisions(int limit);

	void fillDataForReport(int limit, String type, ReportData data);
}
