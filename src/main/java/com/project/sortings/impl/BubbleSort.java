package com.project.sortings.impl;

public class BubbleSort extends SortImpl {

	public BubbleSort(int[] elements, int size) {
		this.setElements(elements);
		this.setSize(size);
	}

	@Override
	public void sortElements() {
		long startTime = System.currentTimeMillis();
		this.setComp(0);
		for (int i = 0; i < this.getSize(); i++) {
			for (int j = 0; j < this.getSize() - i - 1; j++) {
				this.setComp(this.getComp() + 1);
				if (this.getElements()[j] > this.getElements()[j + 1]) {
					int temp = this.getElements()[j];
					this.getElements()[j] = this.getElements()[j + 1];
					this.getElements()[j + 1] = temp;

				}
			}
		}
		long endTime = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		this.setTime((int)totalTime);
		// int seconds = (int) (totalTime / 1000) % 60;
		// int minutes = (int) ((totalTime / (1000 * 60)) % 60);
		// int hours = (int) ((totalTime / (1000 * 60 * 60)) % 24);

	}

}
