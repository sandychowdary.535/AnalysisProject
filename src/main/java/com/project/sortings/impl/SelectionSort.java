package com.project.sortings.impl;

public class SelectionSort extends SortImpl {

	public SelectionSort(int[] elements, int limit) {
		this.setElements(elements);
		this.setSize(limit);
	}

	@Override
	public void sortElements() {
		long startTime = System.currentTimeMillis();
		this.setComp(0);
		for (int i = 0; i < this.getSize() - 1; i++) {
			int min = i;
			for (int j = i + 1; j < this.getSize(); j++) {
				this.setComp(this.getComp() + 1);
				if (this.getElements()[j] < this.getElements()[min]) {
					min = j;
				}
			}
			int temp = this.getElements()[i];
			this.getElements()[i] = this.getElements()[min];
			this.getElements()[min] = temp;
		}
		long endTime = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		this.setTime((int)totalTime);

	}

	public static void main(String[] args) {
		int[] a = { 5, 1, 6, 2, 4, 3 };

		System.out.print("Sorted list is : ");
		for (int i = 0; i < a.length; i++) {
			System.out.print(a[i] + " ");
		}
	}

}
