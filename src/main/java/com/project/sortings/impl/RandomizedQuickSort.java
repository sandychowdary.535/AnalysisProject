package com.project.sortings.impl;

import java.util.Random;

public class RandomizedQuickSort extends SortImpl {
	public int length;
	public int[] array;

	public RandomizedQuickSort(int[] elements, int limit) {
		this.setElements(elements);
		this.setSize(limit);
	}

	@Override
	public void sortElements() {

		this.array = this.getElements();
		this.length = this.getSize();
		long startTime = System.currentTimeMillis();
		this.setComp(0);
		QuickSort(0, length - 1);
		long endTime = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		this.setTime((int)totalTime);

	}

	public void QuickSort(int left, int right) {
		if (right - left <= 0)
			return;
		else {
			Random rand = new Random();
			int pivotIndex = left + rand.nextInt(right - left + 1);
			swap(pivotIndex, right);

			int pivot = array[right];
			int partition = partitionIt(left, right, pivot);
			QuickSort(left, partition - 1);
			QuickSort(partition + 1, right);
		}
	}

	public int partitionIt(int left, int right, long pivot) {
		int leftPtr = left - 1;
		int rightPtr = right;
		while (true) {
			this.setComp(this.getComp() + 1);
			while (array[++leftPtr] < pivot)
				;
			while (rightPtr > 0 && array[--rightPtr] > pivot)
				;

			if (leftPtr >= rightPtr)
				break;
			else
				swap(leftPtr, rightPtr);
		}
		swap(leftPtr, right);
		return leftPtr;
	}

	public void swap(int dex1, int dex2) {
		int temp = array[dex1];
		array[dex1] = array[dex2];
		array[dex2] = temp;
	}

}
