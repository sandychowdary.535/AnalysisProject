package com.project.sortings.impl;

import com.program.pojo.SortData;
import com.program.report.ReportData;
import com.project.sortings.Sort;

public abstract class SortImpl extends SortData implements Sort {

	public abstract void sortElements();

	public void printTotalComparisions(int limit) {
		System.out.println("Number of Comparisions are :" + this.getComp());
	}

	public void printTotalRunningTime(int limit) {
		System.out.println(
				"Total running time to run with " + limit + " elements is:" + this.getTime() + " milliseconds");
	}

	public void fillDataForReport(int limit, String type, ReportData data) {

		switch (limit) {
		case 100:
			if (type == "RAN") {
				data.setRan100Results(this);
			} else if (type == "INC") {
				data.setInc100Results(this);
			} else if (type == "DEC") {
				data.setDec100Results(this);
			}

			break;

		case 1000:
			if (type == "RAN") {
				data.setRan1000Results(this);
			} else if (type == "INC") {
				data.setInc1000Results(this);
			} else if (type == "DEC") {
				data.setDec1000Results(this);
			}
			break;
		case 10000:
			if (type == "RAN") {
				data.setRan10000Results(this);
			} else if (type == "INC") {
				data.setInc10000Results(this);
			} else if (type == "DEC") {
				data.setDec10000Results(this);
			}
			break;
		case 30000:
			if (type == "RAN") {
				data.setRan30000Results(this);
			} else if (type == "INC") {
				data.setInc30000Results(this);
			} else if (type == "DEC") {
				data.setDec30000Results(this);
			}
			break;

		}

	}

	public void printElements() {

		for (int i = 0; i < this.getSize(); i++) {
			if (i % 10 == 0)
				System.out.println();
			System.out.print(this.getElements()[i] + " ");
		}

		System.out.println();
		System.out.println();
		System.out.println();

	}

}
