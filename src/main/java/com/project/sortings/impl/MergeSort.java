package com.project.sortings.impl;

public class MergeSort extends SortImpl {
	private int[] array;
	private int[] tempMergArr;
	private int length;

	public MergeSort(int[] elements, int limit) {
		this.setElements(elements);
		this.setSize(limit);
	}

	@Override
	public void sortElements() {
		this.array = this.getElements();
		this.length = this.getSize();
		this.tempMergArr = new int[length];
		long startTime = System.currentTimeMillis();
		this.setComp(0);
		doMergeSort(0, length - 1);
		long endTime = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		this.setTime((int)totalTime);

	}

	private void doMergeSort(int lowerIndex, int higherIndex) {

		if (lowerIndex < higherIndex) {
			int middle = lowerIndex + (higherIndex - lowerIndex) / 2;
			// Below step sorts the left side of the array
			doMergeSort(lowerIndex, middle);
			// Below step sorts the right side of the array
			doMergeSort(middle + 1, higherIndex);
			// Now merge both sides
			mergeParts(lowerIndex, middle, higherIndex);
		}
	}

	private void mergeParts(int lowerIndex, int middle, int higherIndex) {

		for (int i = lowerIndex; i <= higherIndex; i++) {
			tempMergArr[i] = array[i];
		}
		int i = lowerIndex;
		int j = middle + 1;
		int k = lowerIndex;
		while (i <= middle && j <= higherIndex) {
			this.setComp(this.getComp() + 1);
			if (tempMergArr[i] <= tempMergArr[j]) {
				array[k] = tempMergArr[i];
				i++;
			} else {
				array[k] = tempMergArr[j];
				j++;
			}
			k++;
		}
		while (i <= middle) {
			array[k] = tempMergArr[i];
			k++;
			i++;
		}

	}

}
