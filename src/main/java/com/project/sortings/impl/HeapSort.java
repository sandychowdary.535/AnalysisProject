package com.project.sortings.impl;

public class HeapSort extends SortImpl {
	private int n;

	public HeapSort(int[] elements, int limit) {
		this.setElements(elements);
		this.setSize(limit);
	}

	@Override
	public void sortElements() {
		this.n = this.getSize();
		long startTime = System.currentTimeMillis();
		this.setComp(0);
		heapify(this.getElements());
		for (int i = n; i > 0; i--) {
			swap(this.getElements(), 0, i);
			n = n - 1;
			maxheap(this.getElements(), 0);
		}
		long endTime = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		this.setTime((int)totalTime);
	}

	public void heapify(int arr[]) {
		n = arr.length - 1;
		for (int i = n / 2; i >= 0; i--)
			maxheap(arr, i);
	}

	public void maxheap(int arr[], int i) {
		this.setComp(this.getComp() + 2);
		int left = 2 * i;
		int right = 2 * i + 1;
		int max = i;
		if (left <= n && arr[left] > arr[i])
			max = left;
		if (right <= n && arr[right] > arr[max])
			max = right;

		if (max != i) {
			swap(arr, i, max);
			maxheap(arr, max);
		}
	}

	public void swap(int arr[], int i, int j) {
		int tmp = arr[i];
		arr[i] = arr[j];
		arr[j] = tmp;
	}

}
