package com.project.sortings.impl;

public class InsertionSort extends SortImpl {

	public InsertionSort(int[] elements, int limit) {
		this.setElements(elements);
		this.setSize(limit);
	}



	@Override
	public void sortElements() {
		long startTime = System.currentTimeMillis();
		this.setComp(0);
		for (int i = 1; i < this.getSize(); i++) {
			int key = this.getElements()[i];
			int j = i - 1;
			while (j >= 0 && key < this.getElements()[j]) {
				this.setComp(this.getComp() + 1);
				this.getElements()[j + 1] = this.getElements()[j];
				j--;
			}
			this.getElements()[j + 1] = key;
		}
		long endTime = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		this.setTime((int)totalTime);

	}
	
	public static void main(String[] args) {
		
	}

}
