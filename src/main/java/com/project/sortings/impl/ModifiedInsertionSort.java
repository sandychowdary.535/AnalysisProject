package com.project.sortings.impl;

public class ModifiedInsertionSort extends SortImpl {

	public ModifiedInsertionSort(int[] elements, int limit) {
		this.setElements(elements);
		this.setSize(limit);
	}

	@Override
	public void sortElements() {
		long startTime = System.currentTimeMillis();
		this.setComp(0);
		
		for (int i=0;i<this.getSize();++i){
            int temp=this.getElements()[i];
            int left=0;
            int right=i;
            while (left<right){
                int middle=(left+right)/2;
                this.setComp(this.getComp() + 1);
                if (temp>=this.getElements()[middle])
                    left=right+1;
                else
                    right=middle;
            }
            for (int j=i;j>left;--j){
                swap(this.getElements(),j-1,j);
            }
        }
		long endTime = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		this.setTime((int)totalTime);
	}
	
	public static void swap(int a[],int i,int j){
        int k=a[i];
        a[i]=a[j];
        a[j]=k;
    }
}
