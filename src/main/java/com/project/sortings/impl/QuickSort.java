package com.project.sortings.impl;

public class QuickSort extends SortImpl {
	private int[] array;
	private int length;

	public QuickSort(int[] elements, int limit) {
		this.setElements(elements);
		this.setSize(limit);
	}

	@Override
	public void sortElements() {
		this.array = this.getElements();
		this.length = this.getSize();
		long startTime = System.currentTimeMillis();
		this.setComp(0);
		quickSort(0, length - 1);
		long endTime = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		this.setTime((int)totalTime);

	}

	private void quickSort(int lowerIndex, int higherIndex) {

		int i = lowerIndex;
		int j = higherIndex;
		// calculate pivot number, I am taking pivot as middle index number
		int pivot = array[lowerIndex + (higherIndex - lowerIndex) / 2];
		// Divide into two arrays
		while (i <= j) {
			this.setComp(this.getComp() + 1);
			while (array[i] < pivot) {
				i++;
			}
			while (array[j] > pivot) {
				j--;
			}
			if (i <= j) {
				exchangeNumbers(i, j);
				// move index to next position on both sides
				i++;
				j--;
			}
		}
		// call quickSort() method recursively
		if (lowerIndex < j)
			quickSort(lowerIndex, j);
		if (i < higherIndex)
			quickSort(i, higherIndex);
	}

	private void exchangeNumbers(int i, int j) {
		int temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}

}
