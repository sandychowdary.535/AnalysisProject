package com.project.conole;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import com.program.report.ReportData;
import com.program.report.ReportGenerator;
import com.project.generator.NumberSequenceGenerator;
import com.project.sortings.Sort;
import com.project.sortings.impl.BubbleSort;
import com.project.sortings.impl.HeapSort;
import com.project.sortings.impl.InsertionSort;
import com.project.sortings.impl.MergeSort;
import com.project.sortings.impl.ModifiedInsertionSort;
import com.project.sortings.impl.QuickSort;
import com.project.sortings.impl.RandomizedQuickSort;
import com.project.sortings.impl.SelectionSort;

public class Application {

	private Map<String, ReportData> reports = new HashMap<String, ReportData>();
	private static String type;

	public Application() {
		reports.put("selection", new ReportData());
		reports.put("bubble", new ReportData());
		reports.put("insertion", new ReportData());
		reports.put("modifiedInsertion", new ReportData());
		reports.put("quick", new ReportData());
		reports.put("randomizedQuick", new ReportData());
		reports.put("merge", new ReportData());
		reports.put("heap", new ReportData());
	}

	public void selectionSort(int[] elements, int limit, String type) {
		int[] temp = Arrays.copyOf(elements, elements.length);
		String str = null;
		if (type == "RAN")
			str = "Randomized ordered";
		if (type == "INC")
			str = "Incrementing order";
		if (type == "DEC")
			str = "Decrementing order";
		System.out.println("******************* RUNNING " + str + " SELECTION SORT WITH " + limit
				+ " elements************************");
		Sort s = new SelectionSort(temp, limit);
		System.out.println("Elements before sorting");
		if (limit > 500)
			System.out.println("Elements are too long to print............");
		else
			s.printElements();
		s.sortElements();
		System.out.println("Elements After sorting");
		if (limit > 500)
			System.out.println("Elements are too long to print............");
		else
			s.printElements();
		ReportData data = reports.get("selection");
		s.fillDataForReport(limit, type, data);
		s.printTotalComparisions(limit);
		s.printTotalRunningTime(limit);
		System.out.println();
		System.out.println();
		System.out.println();

	}

	public void bubbleSort(int[] elements, int limit, String type) {
		int[] temp = Arrays.copyOf(elements, elements.length);
		String str = null;
		if (type == "RAN")
			str = "Randomized ordered";
		if (type == "INC")
			str = "Incrementing order";
		if (type == "DEC")
			str = "Decrementing order";
		System.out.println("******************* RUNNING " + str + " BUBBLE SORT WITH " + limit
				+ " elements************************");
		Sort s = new BubbleSort(temp, limit);
		System.out.println("Elements before sorting");
		if (limit > 500)
			System.out.println("Elements are too long to print............");
		else
			s.printElements();
		s.sortElements();
		System.out.println("Elements After sorting");
		if (limit > 500)
			System.out.println("Elements are too long to print............");
		else
			s.printElements();
		ReportData data = reports.get("bubble");
		s.fillDataForReport(limit, type, data);
		s.printTotalComparisions(limit);
		s.printTotalRunningTime(limit);
		System.out.println();
		System.out.println();
		System.out.println();

	}

	public void insertionSort(int[] elements, int limit, String type) {
		int[] temp = Arrays.copyOf(elements, elements.length);
		String str = null;
		if (type == "RAN")
			str = "Randomized ordered";
		if (type == "INC")
			str = "Incrementing order";
		if (type == "DEC")
			str = "Decrementing order";
		System.out.println("******************* RUNNING " + str + " INSERTION SORT WITH " + limit
				+ " elements ************************");
		Sort s = new InsertionSort(temp, limit);
		System.out.println("Elements before sorting");
		if (limit > 500)
			System.out.println("Elements are too long to print............");
		else
			s.printElements();
		s.sortElements();
		System.out.println("Elements After sorting");
		if (limit > 500)
			System.out.println("Elements are too long to print............");
		else
			s.printElements();

		ReportData data = reports.get("insertion");
		s.fillDataForReport(limit, type, data);
		s.printTotalComparisions(limit);
		s.printTotalRunningTime(limit);
		System.out.println();
		System.out.println();

	}

	public void modifiedInsertionSort(int[] elements, int limit, String type) {
		int[] temp = Arrays.copyOf(elements, elements.length);
		String str = null;
		if (type == "RAN")
			str = "Randomized ordered";
		if (type == "INC")
			str = "Incrementing order";
		if (type == "DEC")
			str = "Decrementing order";
		System.out.println("******************* RUNNING " + str + " MODIFIED INSERTION SORT WITH " + limit
				+ " elements ************************");
		Sort s = new ModifiedInsertionSort(temp, limit);
		System.out.println("Elements before sorting");
		if (limit > 500)
			System.out.println("Elements are too long to print............");
		else
			s.printElements();
		s.sortElements();
		System.out.println("Elements After sorting");
		if (limit > 500)
			System.out.println("Elements are too long to print............");
		else
			s.printElements();
		ReportData data = reports.get("modifiedInsertion");
		s.fillDataForReport(limit, type, data);
		s.printTotalComparisions(limit);
		s.printTotalRunningTime(limit);
		System.out.println();
		System.out.println();
		System.out.println();

	}

	public void mergeSort(int[] elements, int limit, String type) {
		int[] temp = Arrays.copyOf(elements, elements.length);
		String str = null;
		if (type == "RAN")
			str = "Randomized ordered";
		if (type == "INC")
			str = "Incrementing order";
		if (type == "DEC")
			str = "Decrementing order";
		System.out.println("******************* RUNNING " + str + " MERGE SORT WITH " + limit
				+ " elements ************************");
		Sort s = new MergeSort(temp, limit);
		System.out.println("Elements before sorting");
		if (limit > 500)
			System.out.println("Elements are too long to print............");
		else
			s.printElements();
		s.sortElements();
		System.out.println("Elements After sorting");
		if (limit > 500)
			System.out.println("Elements are too long to print............");
		else
			s.printElements();
		ReportData data = reports.get("merge");
		s.fillDataForReport(limit, type, data);
		s.printTotalComparisions(limit);
		s.printTotalRunningTime(limit);
		System.out.println();
		System.out.println();
		System.out.println();

	}

	public void quickSort(int[] elements, int limit, String type) {
		int[] temp = Arrays.copyOf(elements, elements.length);
		String str = null;
		if (type == "RAN")
			str = "Randomized ordered";
		if (type == "INC")
			str = "Incrementing order";
		if (type == "DEC")
			str = "Decrementing order";
		System.out.println("******************* RUNNING " + str + " QUICK SORT WITH " + limit
				+ " elements ************************");
		Sort s = new QuickSort(temp, limit);
		System.out.println("Elements before sorting");
		if (limit > 500)
			System.out.println("Elements are too long to print............");
		else
			s.printElements();
		s.sortElements();
		System.out.println("Elements After sorting");
		if (limit > 500)
			System.out.println("Elements are too long to print............");
		else
			s.printElements();
		ReportData data = reports.get("quick");
		s.fillDataForReport(limit, type, data);
		s.printTotalComparisions(limit);
		s.printTotalRunningTime(limit);
		System.out.println();
		System.out.println();
		System.out.println();

	}

	public void randomizedQuickSort(int[] elements, int limit, String type) {
		int[] temp = Arrays.copyOf(elements, elements.length);
		String str = null;
		if (type == "RAN")
			str = "Randomized ordered";
		if (type == "INC")
			str = "Incrementing order";
		if (type == "DEC")
			str = "Decrementing order";
		System.out.println("******************* RUNNING " + str + " RANDOMIZED QUICK SORT WITH " + limit
				+ " elements ************************");
		Sort s = new RandomizedQuickSort(temp, limit);
		System.out.println("Elements before sorting");
		if (limit > 500)
			System.out.println("Elements are too long to print............");
		else
			s.printElements();
		s.sortElements();
		System.out.println("Elements After sorting");
		if (limit > 500)
			System.out.println("Elements are too long to print............");
		else
			s.printElements();
		ReportData data = reports.get("randomizedQuick");
		s.fillDataForReport(limit, type, data);
		s.printTotalComparisions(limit);
		s.printTotalRunningTime(limit);
		System.out.println();
		System.out.println();
		System.out.println();

	}

	public void heapSort(int[] elements, int limit, String type) {
		int[] temp = Arrays.copyOf(elements, elements.length);
		String str = null;
		if (type == "RAN")
			str = "Randomized ordered";
		if (type == "INC")
			str = "Incrementing order";
		if (type == "DEC")
			str = "Decrementing order";
		System.out.println("******************* RUNNING " + str + " HEAP SORT WITH " + limit
				+ " elements ************************");
		Sort s = new HeapSort(temp, limit);
		System.out.println("Elements before sorting");
		if (limit > 500)
			System.out.println("Elements are too long to print............");
		else
			s.printElements();
		s.sortElements();
		System.out.println("Elements After sorting");
		if (limit > 500)
			System.out.println("Elements are too long to print............");
		else
			s.printElements();
		ReportData data = reports.get("heap");
		s.fillDataForReport(limit, type, data);
		s.printTotalComparisions(limit);
		s.printTotalRunningTime(limit);
		System.out.println();
		System.out.println();
		System.out.println();
	}

	public int[] generateElements(Scanner sc) {
		System.out.println("Enter the limit you want : ");
		int limit = sc.nextInt();
		System.out.println("Select series type : ");
		System.out.println("Enter 1 for random series : ");
		System.out.println("Enter 2 for increasing order series : ");
		System.out.println("Enter 3 for decreasing order series : ");
		int optionType = sc.nextInt();
		int[] elements = null;
		switch (optionType) {
		case 1:
			elements = NumberSequenceGenerator.randomSequence(limit);
			type = "RAN";
			break;
		case 2:
			elements = NumberSequenceGenerator.increasingOrder(limit);
			type = "INC";
			break;
		case 3:
			elements = NumberSequenceGenerator.decreasingOrder(limit);
			type = "DEC";
			break;

		}
		return elements;

	}

	private void defaultCase() {
		int[] elements = null;
		// FOR 100 elements
		elements = NumberSequenceGenerator.randomSequence(100);
		this.runEveryThing(elements, "RAN");
		elements = NumberSequenceGenerator.increasingOrder(100);
		this.runEveryThing(elements, "INC");
		elements = NumberSequenceGenerator.decreasingOrder(100);
		this.runEveryThing(elements, "DEC");
		System.out.println();
		System.out.println();
		System.out.println();
		// FOR 1000 elements
		elements = NumberSequenceGenerator.randomSequence(1000);
		this.runEveryThing(elements, "RAN");
		elements = NumberSequenceGenerator.increasingOrder(1000);
		this.runEveryThing(elements, "INC");
		elements = NumberSequenceGenerator.decreasingOrder(1000);
		this.runEveryThing(elements, "DEC");
		System.out.println();
		System.out.println();
		System.out.println();
		// FOR 10000 elements
		elements = NumberSequenceGenerator.randomSequence(10000);
		this.runEveryThing(elements, "RAN");
		elements = NumberSequenceGenerator.increasingOrder(10000);
		this.runEveryThing(elements, "INC");
		elements = NumberSequenceGenerator.decreasingOrder(10000);
		this.runEveryThing(elements, "DEC");
		System.out.println();
		System.out.println();
		System.out.println();
		// FOR 30000 elements
		elements = NumberSequenceGenerator.randomSequence(30000);
		this.runEveryThing(elements, "RAN");
		elements = NumberSequenceGenerator.increasingOrder(30000);
		this.runEveryThing(elements, "INC");
		elements = NumberSequenceGenerator.decreasingOrder(30000);
		this.runEveryThing(elements, "DEC");
		generateReport();

	}

	private void generateReport() {
		ReportGenerator.gernerate(reports);
		System.out.println("********************************************************************************");
		System.out.println("********************************************************************************");
		System.out.println("********************************************************************************");
		System.out.println("********************************************************************************");
		System.out.println("********************************************************************************");
		System.out.println("***************************REPORTS GENERATED************************************");
		System.out.println("********************************************************************************");
		System.out.println("********************************************************************************");
		System.out.println("********************************************************************************");
		System.out.println("********************************************************************************");
		System.out.println("********************************************************************************");

	}

	public void runEveryThing(int elements[], String type) {
		this.selectionSort(elements, elements.length, type);
		this.bubbleSort(elements, elements.length, type);
		this.insertionSort(elements, elements.length, type);
		this.modifiedInsertionSort(elements, elements.length, type);
		this.mergeSort(elements, elements.length, type);
		this.quickSort(elements, elements.length, type);
		this.randomizedQuickSort(elements, elements.length, type);
		this.heapSort(elements, elements.length, type);
	}

	public static void main(String[] args) {
		Application app = new Application();
		Scanner sc = new Scanner(System.in);
		for (;;) {
			System.out.println("Enter your choice.....");
			System.out.println("Enter 0 to run and analyize all sortings and generate report with your input given");
			System.out.println("Enter 1 to run and analyize selection sort ");
			System.out.println("Enter 2 to run and analyize bubble sort ");
			System.out.println("Enter 3 to run and analyize insertion sort ");
			System.out.println("Enter 4 to run and analyize modified insertion sort ");
			System.out.println("Enter 5 to run and analyize merge sort ");
			System.out.println("Enter 6 to run and analyize quick sort ");
			System.out.println("Enter 7 to run and analyize randomized quick sort ");
			System.out.println("Enter 8 to run and analyize heap sort ");
			System.out.println("Enter 9 to numbers to analyize all sortings with default inputs and generate report");
			System.out.println("Enter any other value to exit");
			int option = sc.nextInt();
			int elements[] = null;
			if (option < 0 || option > 9) {
				System.out.println("You are exited from the application ..........................");
				break;
			}
			switch (option) {
			case 0:
				elements = app.generateElements(sc);
				app.runEveryThing(elements, type);
				app.generateReport();
				break;
			case 1:
				elements = app.generateElements(sc);
				app.selectionSort(elements, elements.length, type);
				break;
			case 2:
				elements = app.generateElements(sc);
				app.bubbleSort(elements, elements.length, type);
				break;
			case 3:
				elements = app.generateElements(sc);
				app.insertionSort(elements, elements.length, type);
				break;
			case 4:
				elements = app.generateElements(sc);
				app.modifiedInsertionSort(elements, elements.length, type);
				break;
			case 5:
				elements = app.generateElements(sc);
				app.mergeSort(elements, elements.length, type);
				break;
			case 6:
				elements = app.generateElements(sc);
				app.quickSort(elements, elements.length, type);
				break;
			case 7:
				elements = app.generateElements(sc);
				app.randomizedQuickSort(elements, elements.length, type);
				break;
			case 8:
				elements = app.generateElements(sc);
				app.heapSort(elements, elements.length, type);
				break;
			case 9:
				app.defaultCase();
				break;
			}
		}

		sc.close();
	}

}
